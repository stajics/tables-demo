const jsonServer = require('json-server')
const bodyParser = require('body-parser')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()

server.use(bodyParser.json());
server.use(middlewares);

server.post("/associate", (req, res) => {
  console.log(req.body);
  res.json(req.body);
});
server.use(router);
server.listen(3001, function () {
  console.log('JSON Server is running on PORT 3001');
})
