import React, { Component } from 'react';
// components
import RaisedButton from 'material-ui/RaisedButton';
import TableWithFilter from './components/TableWithFilter';
// styles
import './App.css';
const sourceTableRows = [
  {id: 1, name:'source1', description:'desc source1'},
  {id: 2, name:'source2', description:'desc source2'},
  {id: 3, name:'source3', description:'desc source3'},
  {id: 4, name:'source4', description:'desc source4'},
];

const targetTableRows = [
  {id: 1, name:'target1', description:'desc target1'},
  {id: 2, name:'target2', description:'desc target2'},
  {id: 3, name:'someTarget3', description:'desc someTarget3'},
  {id: 4, name:'target4', description:'desc target4'},
  {id: 5, name:'target5', description:'desc target5'},
  {id: 6, name:'someTarget6', description:'desc someTarget6'},
];

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { sourceTableSelectedRows: [], targetTableSelectedRows: [] };
    this.handleSend = this.handleSend.bind(this);
    this.handleSourceRowSelection = this.handleSourceRowSelection.bind(this);
    this.handleTargetRowSelection = this.handleTargetRowSelection.bind(this);
  }

  async handleSend() {
    const { sourceTableSelectedRows, targetTableSelectedRows } = this.state;
    const body = {
      source: [],
      target: []
    }
    sourceTableSelectedRows.forEach(rowIndex => {
      body.source.push(sourceTableRows[rowIndex]);
    });
    targetTableSelectedRows.forEach(rowIndex => {
      body.target.push(targetTableRows[rowIndex]);
    });
    await fetch('http://localhost:3001/associate/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    });
  }

  handleSourceRowSelection(selectedRows) {
    this.setState({ sourceTableSelectedRows: selectedRows });
  }

  handleTargetRowSelection(selectedRows) {
    this.setState({ targetTableSelectedRows: selectedRows });
  }

  render() {
    return (
      <div className="App">
        <div style={{ display: 'flex', flex: 'none', height: '45%'}}>
          <TableWithFilter
            title="Source"
            selectedRows={this.state.sourceTableSelectedRows}
            onRowSelection={this.handleSourceRowSelection}
            tableHeaderColumns={[
              'Id',
              'Name',
              'Description',
            ]}
            tableRows={sourceTableRows}
          />
        </div>
        <div style={{ display: 'flex', flex: 'none', height: '45%'}}>
          <TableWithFilter
            title="Target"
            selectedRows={this.state.targetTableSelectedRows}
            onRowSelection={this.handleTargetRowSelection}
            tableHeaderColumns={[
              'Id',
              'Name',
              'Description',
            ]}
          tableRows={targetTableRows}
          />
        </div>
        <RaisedButton label="SEND" onClick={this.handleSend} labelColor="#ffffff" backgroundColor="#3b78e7" />
      </div>
    );
  }
}

export default App;
