import React, { PropTypes, Component } from 'react';
// components
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
// style
import './TableWithFilter.css';

const propTypes = {
  title: PropTypes.string,
  tableHeaderColumns: PropTypes.array,
  tableRows: PropTypes.array,
  height: PropTypes.string,
  onRowSelection: PropTypes.func,
};

const defaultProps = {
  height: '100px',
};

class TableWithFilter extends Component {
  constructor(props) {
    super(props);
    this.state = { filter: '' };
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }

  handleFilterChange(e) {
    this.setState({ filter: e.target.value });
  }

  filterRow(row, filter) {
    if(row.name.startsWith(filter)) {
      return 'table-row';
    }
    return 'none';
  }

  render() {
    const { title, tableHeaderColumns, selectedRows, tableRows, onRowSelection, height } = this.props;
    const { filter } = this.state;
    return (
      <div className="TableWithFilter-container">
        <div className="TableWithFilter-filterBar">
          <span>{title}</span>
          <input placeholder="Filter..." value={filter} onChange={this.handleFilterChange} />
        </div>
        <div className="TableWithFilter-table">
          <Table multiSelectable onRowSelection={onRowSelection} style={{ minHeight: height }}>
            <TableHeader displaySelectAll={false}>
              <TableRow>
                {
                  tableHeaderColumns.map((headerColumn, i) =>
                    <TableHeaderColumn key={i}>
                      {headerColumn}
                    </TableHeaderColumn>)
                }
              </TableRow>
            </TableHeader>
            <TableBody showRowHover deselectOnClickaway={false}>
              {
                tableRows.map((row, i) =>
                  <TableRow style={{display: filter ? this.filterRow(row, filter) : 'table-row'}} key={i} selected={selectedRows.includes(i)}>
                    { Object.values(row).map((rowColumnValue, rowIndex) => (
                        <TableRowColumn key={rowIndex}>
                          {rowColumnValue}
                        </TableRowColumn>
                    ))
                    }
                  </TableRow>)
              }
            </TableBody>
          </Table>
        </div>
      </div>
    );
  }
}
TableWithFilter.propTypes = propTypes;
TableWithFilter.defaultProps = defaultProps;

export default TableWithFilter;
