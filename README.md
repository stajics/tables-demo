# Tables demo

### Installation


###### Web App

Clone the repository and run the following commands under your project root:

```
npm i
npm start
```
Browser should open on http://localhost:3000/ automaticly if not navigate to the link.

###### Server

Navigate to mock-server directory.
Run following commands.

```
npm i
npm start
```
Server is running on http://localhost:3001/
